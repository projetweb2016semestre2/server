should = require 'should'
user = require '../src/user.coffee'

describe 'user', () ->
    it 'saves properly', (done) ->
        user.save "ok", "pwd", (err) ->
            should.not.exist err 
            done()
        
    it 'does not save bc missing parameter', (done) ->
        user.save "only name", (err) ->
            should.exist err 
            done()
            
    it 'does not get bc missing parameter', (done) ->
        #user.get (err) ->
            #should.exist err 
        done()
            
    